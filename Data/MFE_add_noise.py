import numpy as np
import h5py
import matplotlib.pyplot as plt

random_seed = 1
rng = np.random.RandomState(random_seed)

hf = h5py.File('MFE_Re600_DATA.h5','r')

t = np.array(hf.get('/t'))
u0 = np.array(hf.get('/u'))

hf.close()

UM = np.mean(u0,0)
u = u0 - UM

# Set a target SNR
target_snr_db = 40
# Calculate signal power and convert to dB 
u_pow = u ** 2
sig_avg_watts = np.mean(u_pow,0)
sig_avg_db = 10 * np.log10(sig_avg_watts)
# Calculate noise according to [2] then convert to watts
noise_avg_db = sig_avg_db - target_snr_db
noise_avg_watts = 10 ** (noise_avg_db / 10)
# Generate an sample of white noise
mean_noise = 0
noise_volts = np.zeros(u_pow.shape)
for i in range(u_pow.shape[1]):
    noise_volts[:,i] = np.random.normal(mean_noise, np.sqrt(noise_avg_watts[i]), len(u_pow[:,0]))
u_noisy = u + noise_volts + UM

fln = 'MFE_Re600_DATA_noiseSNR' + str(target_snr_db) + '.h5' 

hf = h5py.File(fln,'w')
hf.create_dataset('u_nonoise',data=u0)
hf.create_dataset('u',data=u_noisy)
hf.create_dataset('t',data=t)
hf.close()

plt.plot(u_noisy[0:10000,:])
plt.plot(u0[0:10000,:])
plt.show()
