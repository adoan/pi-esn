import numpy as np
from MFE_model import *
from scipy.linalg import *
import scipy.stats
from matplotlib.pyplot import *

# Parameters of the MFE model
Lx = 1.75*np.pi
Lz = 1.2*np.pi

Re = 600. # new Re to get sustained turbulence

alpha = 2.*np.pi/ Lx
beta = np.pi/2.
gamma = 2.*np.pi/ Lz

zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9 = get_MFE_param(alpha,beta,gamma,Re)

# initial parameters for the MFE model
u0 = np.array( [0.129992, -0.0655929, 0.0475706, 0.0329967, 0.0753854, -0.00325098, -0.042364, -0.019685, -0.101453   ])
u0P = np.array(u0[:])
#u0P[0] = u0P[0] + 1e-8

# simulation parameters
dt = 0.25
Tmax = 20000.
Nt = int(Tmax/dt)
t = np.linspace(0.0,Tmax-dt,Nt)

# do transient simulation
uT = np.zeros((8000,9))
uT[0,:] = u0

for i in range(8000-1):
    k1 = dt*MFE_RHS(uT[i,:], zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    k2 = dt*MFE_RHS(uT[i,:]+k1/3., zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    k3 = dt*MFE_RHS(uT[i,:]-k1/3.+k2, zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    k4 = dt*MFE_RHS(uT[i,:]+k1-k2+k3, zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    uT[i+1,:] = uT[i,:] + (k1 + 3.*k2 + 3.*k3 + k4) / 8.

u0 = uT[-1,:]
u0P = np.array(u0[:])
u0P[0] = u0P[0] + 1e-8

# do 2 simulation
u = np.zeros((Nt,9))
u[0,:] = u0

for i in range(Nt-1):
    k1 = dt*MFE_RHS(u[i,:], zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    k2 = dt*MFE_RHS(u[i,:]+k1/3., zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    k3 = dt*MFE_RHS(u[i,:]-k1/3.+k2, zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    k4 = dt*MFE_RHS(u[i,:]+k1-k2+k3, zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    u[i+1,:] = u[i,:] + (k1 + 3.*k2 + 3.*k3 + k4) / 8.
    
    
uP = np.zeros((Nt,9))
uP[0,:] = u0P
for i in range(Nt-1):
    k1 = dt*MFE_RHS(uP[i,:], zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    k2 = dt*MFE_RHS(uP[i,:]+k1/3., zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    k3 = dt*MFE_RHS(uP[i,:]-k1/3.+k2, zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    k4 = dt*MFE_RHS(uP[i,:]+k1-k2+k3, zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    
    uP[i+1,:] = uP[i,:] + (k1 + 3.*k2 + 3.*k3 + k4) / 8.

# Calculate Error
N = np.size(t)
error = norm(np.absolute(u-uP),axis=1)

# show error
plt.plot(t,np.log10(error),'k',linewidth=1)
plt.show()

# Curve fitting
ti = int(1/dt)
tf = int(800/dt)
P = np.polyfit(t[ti:tf],np.log(error[ti:tf]),1)
f = np.poly1d(P)
tFit = np.arange(ti*dt,tf*dt,dt)
errorFit = f(tFit)
slope, R2, _, _, _ = scipy.stats.linregress(t[ti:tf],np.log(error[ti:tf]))
print('Maximal Lyapunov Exponent: %f' % P[0])
# approximately 0.022-0.023, so Lyapunov time of approx 41-45

plt.plot(t[ti:tf],np.log10(error[ti:tf]),'k',linewidth=1)
plt.plot(tFit,errorFit*np.log10(np.e),'k--')
plt.show()
