import h5py
import numpy as np
import matplotlib.pyplot as plt

hf = h5py.File('MFE_Re600_DATA.h5','r')

u = np.array(hf.get('u'))
E = 0.5*np.sum(u**2.,1)
E0 = E


hf.close()
ie=0
i_RE = []
i_e = []
i_b = []
ib = 1
idx = 0
while (ib):
    ib = np.argmax(E>0.1)
    E = E[ib:]
    ie = np.argmax(E<0.1)
    E = E[ie:]
    if (idx>0 and ib!=0):
        ib = ib + i_e[idx-1]
    i_b.append(ib)
    i_RE.append(ib)
    idx = idx + 1
    if idx>0:
        ie = ie + i_b[idx-1]
    i_e.append(ie)

i_RE = i_RE[:-1]
i_RE = np.array(i_RE) - 600
plt.plot(E0)
for i in range(len(i_RE)):
    plt.plot([i_RE[i], i_RE[i]],[0.0,0.5],'r')

plt.show()

# on DATA  
# i_RE = array([ 12932,  15677,  20765,  30400,  34342,  40066,  47130,  48643,
#         50876,  56142,  62604,  65646,  67575,  90336,  93307,
#         96825,  99001, 102369, 127442, 158167, 170532, 172184, 175121,
#        182053, 193514, 202318, 204184, 205484, 228974, 231146, 259660,
#        274823, 275738, 283542, 285766, 293069, 299638,
#        303746, 314061, 330703, 334269, 340136, 354575, 372900, 375667,
#        379363, 382553, 390997, 395101])


