# Short- and long-term prediction of chaotic flows: A physics-constrained reservoir computing approach

This repository contains the code and results related to the manuscript "Short- and long-term prediction of chaotic flows: A physics-constrained reservoir computing approach" by N.A.K. Doan, W. Polifke and L. Magri.

In this work, we propose a physics-informed Echo State Network for the long and short-term prediction of a prototype of turbulent flow, called the MFE system.
We focus on the extreme event prediction and the robustness with respect to noise.

## Prerequisites
The scripts require the librarires: tensorflow 1.x and h5py.
The plotting routines require matlab.

## Description
Data folder: contains the data from the MFE evolution used for the training

Scripts: contains the main scripts:
* noise/: contains the modified version of the scripts to consider noisy training data
* run_ESN.py: design and train the ESN or PI-ESN
* ESN_Chaos.py: contains the ESN class
* calc_err_RE_ESN_rev1.py: computes the extreme event predictions given a model
* calc_longterm_ESN_.py: computes longterm autonomous predictions given a model
 
Results: contains the results shown in the manuscript
* MFE_results: result for non-noisy training data
    * Figures/: contains the raw figures used in the manuscript
    * RE_test/: result from extreme event (or rare events, RE) prediction (subfolder nophys/: ESN; subfolder phys/: PI-ESN; subfolder phys_optim_hyperparam: PI-ESN with optimal hyperparameters)
    * model_nophys/: saved trained ESN models
    * model_phys/: saved trained PI-ESN models
    * model_phys_optim_param:/ saved trained PI-ESN models with optimal hyperparameters
    * LONGTERM_stat/: results of longterm statistics calculations
    * plot_longterm_a_k_pdf.m : plot Fig. 4 (pdf of k and a1 in long term prediction)
    * plot_longterm_a.m : plot Fig. 3 (time-series of a1, a2, a3, k of long term prediction)
    * plot_MFE_2D_comp_ESN.m : plot 2D velocity panels of Fig 7 (prediction of MFE evolution during extreme event by ESN)
    * plot_MFE_2D_comp_PIESN.m : plot 2D velocity panels of Fig 7 (prediction of MFE evolution during extreme event by PI-ESN)
    * plot_MFE_2D_evol_RE.m : plot 2D velocity panels of Fig. 1 (evolution of MFE during extreme event)
    * plot_MFE_LTStats.m : plot velocity statistics (Fig. 5)
    * plot_MFE_RE_pred_ALL.m : plot prediction horizon of extreme events by ESN, PI-ESN and PI-ESN with optimized hyperparameters (Fig. 8)
    * plot_MFE_RE_pred_DETAIL_ALL.m : plot one prediction horizon of extreme events (a1, a2, a3 and error) by ESN and PI-ESN (Fig. 6)
* MFE_results_Noise: result for noisy training data
    * Figures/: contains the raw figures used in the manuscript
    * SNR30/: results for training data with signal-to-noise ratio of 30dB
        * RE_test: result from extreme event (or rare events, RE) prediction (subfolder nophys/: ESN; subfolder phys/: PI-ESN; subfolder phys_optim_hyperparam: PI-ESN with optimal hyperparameters)
        * model_nophys/: saved trained ESN models
        * model_phys/: saved trained PI-ESN models
        * LONGTERM_stat/: results of longterm statistics calculations
    * SNR40/: results for training data with signal-to-noise ratio of 40dB
        * RE_test: result from extreme event (or rare events, RE) prediction (subfolder nophys/: ESN; subfolder phys/: PI-ESN; subfolder phys_optim_hyperparam: PI-ESN with optimal hyperparameters)
        * model_nophys/: saved trained ESN models
        * model_phys/: saved trained PI-ESN models
        * LONGTERM_stat/: results of longterm statistics calculations
    * plot_MFE_LTStats.m : plot velocity statistics (Fig. 11)
    * plot_MFE_RE_pred_ALL.m : plot prediction horizon of extreme events by ESN, PI-ESN (Fig. 10)
    * plot_MFE_RE_pred_detail.m : plot one prediction horizon of extreme events (a1, a2, a3 and error) by ESN and PI-ESN (Fig. 9)

## Contact
Please contact Anh Khoa Doan (n.a.k.doan@tudelft.nl) for any question regarding this repository.
