clear all; close all; clc

num_units_list = 500:250:3000;
valid_hor_list = [5000];
SNR_list = [30 40];
col = [0.25 0.25 1; 1 0 0];
linst = {'-', '--'};

for iSNR =1:2
    SNR = SNR_list(iSNR);
    for inum = 1:length(num_units_list)
        num_units = num_units_list(inum);
        for k=1:length(valid_hor_list)
            validhor = valid_hor_list(k);
            fln = ['./SNR' num2str(SNR) '/RE_test/phys/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-06units_phys_validhor_' num2str(validhor) '_RE.h5'];
            time_thres = 0.2;
            for i_RE = 0:48
                Y = h5read(fln,['/Ypred' num2str(i_RE)]);
                Ydata = h5read(fln,['/data' num2str(i_RE)]);
                E = sqrt( sum((Ydata-Y).^2,1) )./ sqrt( mean ( sum(Ydata.^2,1)));
                valid_time_phys(inum,k,i_RE+1) = find(E>time_thres,1)*0.25;
            end
        end
    end

    valid_RE_ESNphys = squeeze(valid_time_phys(:,1,:));

    %%

    for inum = 1:length(num_units_list)
        num_units = num_units_list(inum);
        fln = ['./SNR' num2str(SNR) '/RE_test/nophys/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-06units_nophys_RE.h5'];
        
        time_thres = 0.2;
        for i_RE = 0:48
            Y = h5read(fln,['/Ypred' num2str(i_RE)]);
            Ydata = h5read(fln,['/data' num2str(i_RE)]);
            E = sqrt( sum((Ydata-Y).^2,1) )./ sqrt( mean ( sum(Ydata.^2,1)));
            valid_time_nophys(inum,i_RE+1) = find(E>time_thres,1)*0.25;
        end

    end
    valid_RE_ESNnophys = squeeze(valid_time_nophys);

    %%
    % close all
    figure(2)
    plot(num_units_list,mean(valid_RE_ESNphys,2)/41,'x','Color',col(1,:),'LineWidth',1.5,'LineStyle',linst{iSNR})
    hold on
    plot(num_units_list,mean(valid_RE_ESNnophys,2)/41,'o','Color',col(2,:),'LineWidth',1.5,'LineStyle',linst{iSNR})
end

xlabel('$N_x$','Interpreter','latex','FontSize',14)
ylabel('Predictability horizon','Interpreter','latex','FontSize',14)
set(gca,'TickLabelInterpreter','latex','FontSize',12)
set(gcf,'Units','centimeters','Position',[0 0 12 6])
ylim([1.5 3.0])
