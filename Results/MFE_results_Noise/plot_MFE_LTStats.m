clear all; close all; clc;

%%
fln = '../../Data/MFE_Re600_DATA.h5';
alpha = h5read(fln,'/alpha');
beta = h5read(fln,'/beta');
gamma = h5read(fln,'/gamma');
Lx = h5read(fln,'/Lx');
Lz = h5read(fln,'/Lz');
Ly = 2;

%%
num_units_list = [1500];
linest = {'-', ':'};
SNRl = [30,40];
for iSNR = 1:length(SNRl)
    SNR = SNRl(iSNR);
    for inum=1:length(num_units_list)
        num_units = num_units_list(inum);
        fln_ESN = ['./SNR' num2str(SNR) '/LONGTERM_stat/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-06units_phys_validhor_5000_STATS.h5'];

        uM_ESN_P = h5read(fln_ESN,'/uM');
        uvM_ESN_P = h5read(fln_ESN,'/uvM');
        uM2_ESN_P = h5read(fln_ESN,'/uM2');
        uM3_ESN_P = h5read(fln_ESN,'/uM3');
        uM4_ESN_P = h5read(fln_ESN,'/uM4');
        fln_ESN = ['./SNR' num2str(SNR) '/LONGTERM_stat/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-06units_nophys_STATS.h5'];


        uM_ESN_NP = h5read(fln_ESN,'/uM');
        uvM_ESN_NP = h5read(fln_ESN,'/uvM');
        uM2_ESN_NP = h5read(fln_ESN,'/uM2');
        uM3_ESN_NP = h5read(fln_ESN,'/uM3');
        uM4_ESN_NP = h5read(fln_ESN,'/uM4');
        
        if iSNR==1
            fln = ['./SNR' num2str(SNR) '/LONGTERM_stat/MFE_ESN_REF_STATS.h5'];
            uM = h5read(fln,'/uM');
            uvM = h5read(fln,'/uvM');
            uM2 = h5read(fln,'/uM2');
            uM3 = h5read(fln,'/uM3');
            uM4 = h5read(fln,'/uM4');
        end

    %%
        [Nx,Ny,Nz,~] = size(uM);
        yy = linspace(-1,1,Ny);
        figure(1)
        uMM = squeeze(mean(mean(uM,1),3));
        if inum==1
            plot(uMM(:,1),yy,'Color','k','LineWidth',2.5)
        end
        hold on
        uMM_ESN_P = squeeze(mean(mean(uM_ESN_P,1),3));
        plot(uMM_ESN_P(:,1),yy,'Color',[0.25 0.25 1],'LineWidth',1.6,'LineStyle',linest{iSNR})
        uMM_ESN_NP = squeeze(mean(mean(uM_ESN_NP,1),3));
        plot(uMM_ESN_NP(:,1),yy,'Color','r','LineWidth',1.1,'LineStyle',linest{iSNR})
        set(gca,'TickLabelInterpreter','latex','FontSize',12)
        xlabel('$\overline{u}$','FontSize',14,'Interpreter','latex')
        ylabel('$y$','FontSize',14,'Interpreter','latex')
        set(gcf,'Units','Centimeters','Position',[0 0 6 6])

        figure(2)
        uMM2 = squeeze(mean(mean(uM2,1),3));
        if inum==1
            plot(uMM2(:,1),yy,'Color','k','LineWidth',2.5)
        end
        hold on
        uMM2_ESN_P = squeeze(mean(mean(uM2_ESN_P,1),3));
        plot(uMM2_ESN_P(:,1),yy,'Color',[0.25 0.25 1],'LineWidth',1.6,'LineStyle',linest{iSNR})
        uMM2_ESN_NP = squeeze(mean(mean(uM2_ESN_NP,1),3));
        plot(uMM2_ESN_NP(:,1),yy,'Color','r','LineWidth',1.1,'LineStyle',linest{iSNR})
        set(gca,'TickLabelInterpreter','latex','FontSize',12)
        xlabel('$\overline{u^2}$','FontSize',14,'Interpreter','latex')
        ylabel('$y$','FontSize',14,'Interpreter','latex')
        set(gcf,'Units','Centimeters','Position',[0 0 6 6])

        figure(5)
        uvMM = squeeze(mean(mean(uvM,1),3));
        if inum==1
            plot(uvMM,yy,'Color','k','LineWidth',2.5)
        end
        hold on
        uvMM_ESN_P = squeeze(mean(mean(uvM_ESN_P,1),3));
        plot(uvMM_ESN_P,yy,'Color',[0.25 0.25 1],'LineWidth',1.6,'LineStyle',linest{iSNR})
        uvMM_ESN_NP = squeeze(mean(mean(uvM_ESN_NP,1),3));
        plot(uvMM_ESN_NP,yy,'Color','r','LineWidth',1.1,'LineStyle',linest{iSNR})
        set(gca,'TickLabelInterpreter','latex','FontSize',12)
        xlabel('$\overline{uv}$','FontSize',14,'Interpreter','latex')
        ylabel('$y$','FontSize',14,'Interpreter','latex')
        set(gcf,'Units','Centimeters','Position',[0 0 6 6])

    end
end

%%
for i=[1,2,5]
    set(i,'Units','Centimeters','Position',[0 0 7 7])
end
figure(1)
xlim([-0.5 0.5])
figure(2)
xlim([0 0.22])
figure(5)
xlim([-5 1]*0.001)
