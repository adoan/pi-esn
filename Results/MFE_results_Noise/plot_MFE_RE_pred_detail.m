clear all; close all; clc

%%

LyapT = 41;
dt = 0.25;

Ydata = h5read('MFE_Noise_pred_detail.h5','/data');
Y_phys = h5read('MFE_Noise_pred_detail.h5','/Y_phys');
Y_nophys = h5read('MFE_Noise_pred_detail.h5','/Y_nophys');

ifig = 1;
for j=[1,2,3] 
    figure(ifig)
    plot([0:length(Y_phys(j,:))-1]*0.25/LyapT,Y_phys(j,:),'Color',[0.25 0.25 1],'LineWidth',1.5)
    hold on
    plot([0:length(Y_phys(j,:))-1]*0.25/LyapT,Ydata(j,:),'k','LineWidth',1.7)
    ifig = ifig+1;
    plot([0:length(Y_nophys(j,:))-1]*0.25/LyapT,Y_nophys(j,:),'Color','r','LineWidth',1.4,'LineStyle','--')
end
figure(6)
E_phys = sqrt( sum((Ydata-Y_phys).^2,1) )./ sqrt( mean ( sum(Ydata.^2,1)));
E_nophys = sqrt( sum((Ydata-Y_nophys).^2,1) )./ sqrt( mean ( sum(Ydata.^2,1)));

plot([0:length(Y_phys(j,:))-1]*0.25/LyapT,E_phys,'Color',[0.25 0.25 1],'LineWidth',1.5)
hold on
plot([0:length(Y_nophys(j,:))-1]*0.25/LyapT,E_nophys,'Color','r','LineWidth',1.4,'LineStyle','--')

%%
for i=[1,2,3,6]
    figure(i)
    xlim([0 7])
    xlabel('$t^+$','Interpreter','latex','FontSize',14)
    set(i,'Units','Centimeters','Position',[0 0 10 6])
    set(gca,'TickLabelInterpreter','latex','FontSize',12)
end
