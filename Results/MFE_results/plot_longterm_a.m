clear all; close all; clc;

LyapT = 41;

%%
close all
num_units_list = [1500];
linest = {'--', '-.', ':'};
for inum=1:length(num_units_list)
    num_units = num_units_list(inum);
    fln_ESN = ['./LONGTERM_stat/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-06units_phys_validhor_5000_LTevol.h5'];

    a = h5read(fln_ESN,'/data');
    k = 0.5*sum(a.^2,1);
    a_ESN_P = h5read(fln_ESN,'/Ypred');
    k_ESN_P = 0.5*sum(a_ESN_P.^2,1);
    
    fln_ESN = ['./LONGTERM_stat/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-06units_nophys_LTevol.h5'];

    a_ESN_NP = h5read(fln_ESN,'/Ypred');
    
    k_ESN_NP = 0.5*sum(a_ESN_NP.^2,1);
    
%     t = 0:0.25:0.25*(size(a,2)-1);
    Nt = 20000;
    t = 0:0.25:0.25*(Nt-1);
    t = t/LyapT;
    figure(1)
    plot(t,a(1,1:Nt),'LineWidth',1.5,'Color','k');
    hold on
    plot(t,a_ESN_P(1,1:Nt),'Color',[0.25 0.25 1],'LineWidth',1.3)
    plot(t,a_ESN_NP(1,1:Nt),'Color','r','LineWidth',1.1,'LineStyle','--')

    figure(2)
    plot(t,a(2,1:Nt),'LineWidth',1.5,'Color','k');
    hold on
    plot(t,a_ESN_P(2,1:Nt),'Color',[0.25 0.25 1],'LineWidth',1.3)
    plot(t,a_ESN_NP(2,1:Nt),'Color','r','LineWidth',1.1,'LineStyle','--')
    
    figure(3)
    plot(t,a(3,1:Nt),'LineWidth',1.5,'Color','k');
    hold on
    plot(t,a_ESN_P(3,1:Nt),'Color',[0.25 0.25 1],'LineWidth',1.3)
    plot(t,a_ESN_NP(3,1:Nt),'Color','r','LineWidth',1.1,'LineStyle','--')
    
    figure(4)
    plot(t,k(1,1:Nt),'LineWidth',1.5,'Color','k');
    hold on
    plot(t,k_ESN_P(1,1:Nt),'Color',[0.25 0.25 1],'LineWidth',1.3)
    plot(t,k_ESN_NP(1,1:Nt),'Color','r','LineWidth',1.1,'LineStyle','--')
end
%
for i=1:4
    figure(i)
    xlim([0 max(t)])
    xlabel('$t^+$','Interpreter','latex','FontSize',14)
%     ylabel('$k$','Interpreter','latex','FontSize',14)
    set(i,'Units','Centimeters','Position',[0 0 11 5])
    set(gca,'TickLabelInterpreter','latex','FontSize',12)
%     xlim([0 max(t)])
end
