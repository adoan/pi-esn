clear all; close all; clc

num_units_list = [500 750 1000 1250 1500 1750 2000 2250 2500 2750 3000];
valid_hor_list = [5000];
time_thres = 0.2;
for inum = 1:length(num_units_list)
    num_units = num_units_list(inum);
    for k=1:length(valid_hor_list)
        validhor = valid_hor_list(k);
        fln = ['./RE_test/phys/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-06units_phys_validhor_' num2str(validhor) '_RE.h5'];

        for i_RE = 0:48
            Y = h5read(fln,['/Ypred' num2str(i_RE)]);
            Ydata = h5read(fln,['/data' num2str(i_RE)]);
            E = sqrt( sum((Ydata-Y).^2,1) )./ sqrt( mean ( sum(Ydata.^2,1)));
            valid_time_phys(inum,k,i_RE+1) = find(E>time_thres,1)*0.25;
        end
    end
end

valid_RE_ESNphys = squeeze(valid_time_phys(:,1,:));

%%
for inum = 1:length(num_units_list)
    num_units = num_units_list(inum);
    fln = ['./RE_test/nophys/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-06units_nophys_RE.h5'];
    for i_RE = 0:48
        Y = h5read(fln,['/Ypred' num2str(i_RE)]);
        Ydata = h5read(fln,['/data' num2str(i_RE)]);
        E = sqrt( sum((Ydata-Y).^2,1) )./ sqrt( mean ( sum(Ydata.^2,1)));
        valid_time_nophys(inum,i_RE+1) = find(E>time_thres,1)*0.25;
    end
end

valid_RE_ESNnophys = squeeze(valid_time_nophys);

%%
for inum = 1:length(num_units_list)
    num_units = num_units_list(inum);
    fln = ['./RE_test/nophys/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-05units_nophys_RE.h5'];
    for i_RE = 0:48
        Y = h5read(fln,['/Ypred' num2str(i_RE)]);
        Ydata = h5read(fln,['/data' num2str(i_RE)]);
        E = sqrt( sum((Ydata-Y).^2,1) )./ sqrt( mean ( sum(Ydata.^2,1)));
        valid_time_nophys_lmb5(inum,i_RE+1) = find(E>time_thres,1)*0.25;
    end
    
end
valid_RE_ESNnophys_lmb5 = squeeze(valid_time_nophys_lmb5);

%%
for inum = 1:length(num_units_list)
    num_units = num_units_list(inum);
    fln = ['./RE_test/nophys/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-07units_nophys_RE.h5'];
    for i_RE = 0:48
        Y = h5read(fln,['/Ypred' num2str(i_RE)]);
        Ydata = h5read(fln,['/data' num2str(i_RE)]);
        E = sqrt( sum((Ydata-Y).^2,1) )./ sqrt( mean ( sum(Ydata.^2,1)));
        valid_time_nophys_lmb7(inum,i_RE+1) = find(E>time_thres,1)*0.25;
    end
    
end
valid_RE_ESNnophys_lmb7 = squeeze(valid_time_nophys_lmb7);

%%
for iunits = 1:length(num_units_list)
    num_units = num_units_list(iunits);
    fln = ['./RE_test/phys_optim_hyperparam/MFE_ESN_train' num2str(10000) '_' num2str(num_units) '_lmb1e-06' ...
        'units_phys_validhor_' num2str(validhor) '_RE.h5'];
    for i_RE = 0:48
        Y = h5read(fln,['/Ypred' num2str(i_RE)]);
        Ydata = h5read(fln,['/data' num2str(i_RE)]);
        E = sqrt( sum((Ydata-Y).^2,1) )./ sqrt( mean ( sum(Ydata.^2,1)));
        ttt = find(E>time_thres,1);
        if isempty(ttt)
            valid_time(iunits,i_RE+1) = length(E)*0.25;
        else
            valid_time(iunits,i_RE+1) = ttt*0.25;
        end
    end
end
valid_RE_ESNphysOpt = valid_time;

%%
close all
figure(1)
errorbar(num_units_list,mean(valid_RE_ESNphys,2)/41,0.5*std(valid_RE_ESNphys,1,2)/41,'x-','Color',[0.25 0.25 1.0],'LineWidth',1.5)
hold on
errorbar(num_units_list,mean(valid_RE_ESNphysOpt,2)/41,0.5*std(valid_RE_ESNphysOpt,1,2)/41,'*-','Color',[0 0.6 0],'LineWidth',1.5)

errorbar(num_units_list,mean(valid_RE_ESNnophys,2)/41,0.5*std(valid_RE_ESNnophys,1,2)/41,'o--','Color','r','LineWidth',1.3)

plot(num_units_list,mean(valid_RE_ESNnophys_lmb5,2)/41,'^:','Color','r','LineWidth',1.2)
plot(num_units_list,mean(valid_RE_ESNnophys_lmb7,2)/41,'v-.','Color','r','LineWidth',1.2)
xlabel('$N_x$','Interpreter','latex','FontSize',14)
ylabel('Predictability horizon','Interpreter','latex','FontSize',14)
set(gca,'TickLabelInterpreter','latex','FontSize',12)
set(gcf,'Units','centimeters','Position',[0 0 12 6])
ylim([2. inf])
