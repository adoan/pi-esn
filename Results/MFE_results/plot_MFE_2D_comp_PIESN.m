clear all; close all; clc;

%%
fln = '../../Data/MFE_Re600_DATA.h5';

alpha = h5read(fln,'/alpha');
beta = h5read(fln,'/beta');
gamma = h5read(fln,'/gamma');
Lx = h5read(fln,'/Lx');
Lz = h5read(fln,'/Lz');
Ly = 2;
a = h5read(fln,'/u');

Nt = size(a,2);

%%
num_units = 3000;
fln_ESN = ['./RE_test/phys/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-06units_phys_validhor_5000_RE.h5'];
a_ESN = h5read(fln_ESN,'/Ypred6');
a = h5read(fln_ESN,'/data6');

Nt = size(a,2);

%%
Nx = 20; Nz = 20; Ny = 1;

xx = linspace(0,Lx,Nx);
zz = linspace(0,Lz,Nz);
yy = 0.0;

%%
N8 = 2*sqrt(2) / sqrt((alpha^2+gamma^2)*(4*alpha^2+4*gamma^2+pi^2) );
uu=zeros(3,9);

% close all
u = zeros(Nx,Nz,Ny,3,1);
u_ESN = zeros(Nx,Nz,Ny,3,1);
k=1;
for it=[351:175:701]
for iy = 1
    for ix = 1:length(xx)
        for iz = 1:length(zz)
            x = xx(ix);
            z = zz(iz);
            y = yy(iy);
            uu(:,1) = [ sqrt(2) * sin(pi*y/2); 0; 0];
            uu(:,2) = [4/sqrt(3)* (cos(pi*y/2)).^2 * cos(gamma*z); 0; 0];
            uu(:,3) = (2/sqrt(4*gamma^2 + pi^2) ) * [0; 2*gamma*cos(pi*y/2)*cos(pi*z/2); pi*sin(pi*y/2)*sin(gamma*z) ];
            uu(:,4) = [0; 0; 4/sqrt(3) * cos(alpha*x) * (cos(pi*y/2))^2];
            uu(:,5) = [0; 0; 2*sin(alpha*x)*sin(pi*y/2)];
            uu(:,6)  = ( (4*sqrt(2))/sqrt(3*(alpha^2+gamma^2)) ) * [-gamma*cos(alpha*x)*(cos(pi*y/2))^2*sin(gamma*z); 0; alpha*sin(alpha*x)*(cos(pi*y/2))^2*cos(gamma*z)];
            uu(:,7)  = ( (2*sqrt(2))/sqrt(alpha^2+gamma^2) ) * [gamma*sin(alpha*x)*sin(pi*y/2)*sin(gamma*z); 0; alpha*cos(alpha*x)*sin(pi*y/2)*cos(gamma*z)];

            uu(:,8)  = N8* [pi*alpha*sin(alpha*x)*sin(pi*y/2)*sin(gamma*z);
                2*(alpha^2+gamma^2)*cos(alpha*x)*cos(pi*y/2)*sin(gamma*z);
                -pi*gamma*cos(alpha*x)*sin(pi*y/2)*cos(gamma*z)];
            uu(:,9)  = [sqrt(2)*sin(3*pi*y/2); 0; 0];
            
            u(ix,iz,iy,:,1) = uu*a(:,it);
            u_ESN(ix,iz,iy,:,1) = uu*a_ESN(:,it);
            u_norm = sqrt(u(:,:,:,1).^2 + u(:,:,:,2).^2 + u(:,:,:,3).^2);
            du_ESN = abs(u-u_ESN);
            ux = abs(u(:,:,1,1));
            uy = abs(u(:,:,1,2));
            uz = abs(u(:,:,1,3));
        end
    end
    
end
figure(k)
contourf(xx,zz,squeeze(u_ESN(:,:,1,2,1)))
hold on
quiver(xx(2:2:end-1),zz(4:2:end-3),squeeze(u_ESN(4:2:end-3,2:2:end-1,1,1,1)), squeeze(u_ESN(4:2:end-3,2:2:end-1,1,3,1)), 'r')
% hc = colorbar;
% set(hc,'TickLabelInterpreter','latex','FontSize',12)
caxis([-0.05 0.05])
xlim([xx(1) xx(end)])
ylim([zz(1) zz(end)])
axis equal
set(gca,'XTick','','YTick','')

figure(10+k)
contourf(xx,zz,squeeze(du_ESN(:,:,1,2)))
xlabel('$x$','Interpreter','latex','FontSize',18)
ylabel('$z$','Interpreter','latex','FontSize',18)
hold on
quiver(xx(2:2:end-1),zz(4:2:end-3),squeeze(du_ESN(4:2:end-3,2:2:end-1,1,1)), squeeze(du_ESN(4:2:end-3,2:2:end-1,1,3)), 'r')
% hc = colorbar;
% set(hc,'TickLabelInterpreter','latex','FontSize',12)
caxis([0.0 0.1])
xlim([xx(1) xx(end)])
ylim([zz(1) zz(end)])
axis equal
set(gca,'XTick','','YTick','')


k=k+1;
end
%%
for i=1:k-1
    set(i,'Units','centimeters','Position',[0 0 9 8])
end

for i=1:k-1
    set(10+i,'Units','centimeters','Position',[0 0 9 8])
end


