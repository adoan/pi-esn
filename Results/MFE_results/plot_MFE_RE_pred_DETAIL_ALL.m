clear all; close all; clc

%%

LyapT = 41;


%%
num_units_list = [3000];
valid_hor_list = [5000];
for inum = 1:length(num_units_list)
    num_units = num_units_list(inum);
    fln = ['RE_test/phys/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-06units_phys_validhor_' num2str(5000) '_RE.h5'];
    time_thres = 0.2;
    for i_RE = [6]
        Y = h5read(fln,['/Ypred' num2str(i_RE)]);
        Ydata = h5read(fln,['/data' num2str(i_RE)]);
        E = sqrt( sum((Ydata-Y).^2,1) )./ sqrt( mean ( sum(Ydata.^2,1)));
        valid_time_phys(inum,i_RE+1) = find(E>time_thres,1)*0.25;
        ifig = 1;
        Energy = 0.5*sum(Y.^2,1);
        Energydata = 0.5*sum(Ydata.^2,1);
        for j=[1,2,3]
            figure(ifig)
            plot([0:length(Y(j,:))-1]*0.25/LyapT,Y(j,:),'Color','b','LineWidth',1.5)
            hold on
            plot([0:length(Y(j,:))-1]*0.25/LyapT,Ydata(j,:),'k','LineWidth',1.7)
            ifig = ifig+1;
        end
        figure(4)
        plot([0:length(Y(j,:))-1]*0.25/LyapT,E,'Color','b','LineWidth',1.5)
    end
end

valid_RE_ESNphys = squeeze(valid_time_phys(:,:,:));

%%
for inum = 1:length(num_units_list)
    num_units = num_units_list(inum);
    fln = ['./RE_test/nophys/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-06units_nophys_RE.h5'];
    time_thres = 0.2;
    for i_RE = [6]
        Y = h5read(fln,['/Ypred' num2str(i_RE)]);
        Ydata = h5read(fln,['/data' num2str(i_RE)]);
        E = sqrt( sum((Ydata-Y).^2,1) )./ sqrt( mean ( sum(Ydata.^2,1)));
        valid_time_nophys(inum,i_RE+1) = find(E>time_thres,1)*0.25;
        Energy = 0.5*sum(Y.^2,1);
        ifig = 1;
        for j=[1,2,3]
            figure(ifig)
            plot([0:length(Y(j,:))-1]*0.25/LyapT,Y(j,:),'Color','r','LineWidth',1.4,'LineStyle','--')
            ifig = ifig+1;
        end
        figure(4)
        hold on
        plot([0:length(Y(j,:))-1]*0.25/LyapT,E,'Color','r','LineWidth',1.4,'LineStyle','--')
    end
end
valid_RE_ESNnophys = squeeze(valid_time_nophys);

%%
for i=1:4
    figure(i)
    xlim([0 7])
    xlabel('$t\lambda_{\max}$','Interpreter','latex','FontSize',14)
%     ylabel('$k$','Interpreter','latex','FontSize',14)
    set(i,'Units','Centimeters','Position',[0 0 10 6])
    set(gca,'TickLabelInterpreter','latex','FontSize',12)
%     xlim([0 max(t)])
end

