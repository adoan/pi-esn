clear all; close all; clc;

LyapT = 41;
%
num_units_list = [1500];
for inum=1:length(num_units_list)
    num_units = num_units_list(inum);
    fln_ESN = ['./LONGTERM_stat/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-06units_phys_validhor_5000_LTevol.h5'];

    a = h5read(fln_ESN,'/data');
    k = 0.5*sum(a.^2,1);
    a_ESN_P = h5read(fln_ESN,'/Ypred');
    k_ESN_P = 0.5*sum(a_ESN_P.^2,1);
    
    fln_ESN = ['./LONGTERM_stat/MFE_ESN_train10000_' num2str(num_units) '_lmb1e-06units_nophys_LTevol.h5'];

    a_ESN_NP = h5read(fln_ESN,'/Ypred');
    
    k_ESN_NP = 0.5*sum(a_ESN_NP.^2,1);
    
%     t = 0:0.25:0.25*(size(a,2)-1);
    Nt = 20000;
    t = 0:0.25:0.25*(Nt-1);
    t = t/LyapT;
end
%

k_min = 0.01;
k_max = 0.19;
dk = 0.005;
k_edges = k_min:dk:k_max;
pdf_k = histogram(k(1,:),k_edges);
pdf_k = pdf_k.BinCounts;
k_x = (k_edges(1:end-1) + k_edges(2:end))/2;
pdf_k = pdf_k / (sum(pdf_k)*dk);

pdf_k_ESN_P = histogram(k_ESN_P(1,:),k_edges);
pdf_k_ESN_P = pdf_k_ESN_P.BinCounts;
pdf_k_ESN_P = pdf_k_ESN_P / (sum(pdf_k_ESN_P)*dk);


pdf_k_ESN_NP = histogram(k_ESN_NP(1,:),k_edges);
pdf_k_ESN_NP = pdf_k_ESN_NP.BinCounts;
pdf_k_ESN_NP = pdf_k_ESN_NP / (sum(pdf_k_ESN_NP)*dk);

%
a1_min = 0.045;
a1_max = 0.60;
da1 = 0.005;
a1 = a(1,:);
a1_edges = a1_min:da1:a1_max;
pdf_a1 = histogram(a1(1,:),a1_edges);
pdf_a1 = pdf_a1.BinCounts;
a1_x = (a1_edges(1:end-1) + a1_edges(2:end))/2;
pdf_a1 = pdf_a1 / (sum(pdf_a1)*da1);

a1_ESN_P = a_ESN_P(1,:);
pdf_a1_ESN_P = histogram(a1_ESN_P(1,:),a1_edges);
pdf_a1_ESN_P = pdf_a1_ESN_P.BinCounts;
pdf_a1_ESN_P = pdf_a1_ESN_P / (sum(pdf_a1_ESN_P)*da1);

a1_ESN_NP = a_ESN_NP(1,:);
pdf_a1_ESN_NP = histogram(a1_ESN_NP(1,:),a1_edges);
pdf_a1_ESN_NP = pdf_a1_ESN_NP.BinCounts;
pdf_a1_ESN_NP = pdf_a1_ESN_NP / (sum(pdf_a1_ESN_NP)*da1);

%% close all
close(1)
figure(10)
semilogy(k_x,smooth(pdf_k),'LineWidth',1.5,'Color','k');
hold on
semilogy(k_x,smooth(pdf_k_ESN_P),'Color',[0.25 0.25 1],'LineWidth',1.3);
semilogy(k_x,smooth(pdf_k_ESN_NP),'Color','r','LineWidth',1.1,'LineStyle','--');

set(10,'Units','centimeters','Position',[0 0 10 6])
set(gca,'TickLabelInterpreter','latex','FontSize',12)
xlabel('$k$','Interpreter','latex','FontSize',14)
ylabel('$p(k)$','Interpreter','latex','FontSize',14)

figure(20)
semilogy(a1_x,smooth(pdf_a1),'LineWidth',1.5,'Color','k');
hold on
semilogy(a1_x,smooth(pdf_a1_ESN_P),'Color',[0.25 0.25 1],'LineWidth',1.3);
semilogy(a1_x,smooth(pdf_a1_ESN_NP),'Color','r','LineWidth',1.1,'LineStyle','--');

set(20,'Units','centimeters','Position',[0 0 10 6])
set(gca,'TickLabelInterpreter','latex','FontSize',12)
xlabel('$a_1$','Interpreter','latex','FontSize',14)
ylabel('$p(a_1)$','Interpreter','latex','FontSize',14)