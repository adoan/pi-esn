# NAKD
"""
Last Update: 20/07/2021
Script to read an existing ESN info and run for prediction of extreme events in MFE system

"""

import numpy as np
import tensorflow as tf
from tensorflow.python.ops import math_ops
from ESN_Chaos import EchoStateRNNCell
import h5py

import matplotlib.pyplot as plt


# Configs ----------------------------------------------------------------------

# takes only current needed GPU memory
config = tf.ConfigProto()
#config.gpu_options.allow_growth = True
random_seed = 1

### -------- DATASET ------------------- 

## for MFE model
hf = h5py.File('../Data/MFE_Re600_DATA.h5','r') 
case = 'MFE'
u_exact = np.array(hf.get('u'))
t = np.array(hf.get('t'))
dt = np.array(hf.get('dt'))

# physical parameters for the MFE model
Lx = np.array(hf.get('Lx'))
Lz = np.array(hf.get('Lz'))
alpha = np.array(hf.get('alpha'))
beta = np.array(hf.get('beta'))
gamma_mod = np.array(hf.get('gamma'))
zeta = np.array(hf.get('zeta'))
xiall = []
for i in range(1,10):
    var = 'xi' + str(i)
    xiall.append( np.array(hf.get(var)) )
    
[xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9] = xiall
del xiall

hf.close()

# Extreme events index in DATA
i_RE = [ 12932,  15677,  20765,  30400,  34342,  40066,  47130,  48643,
        50876,  56142,  62604,  65646,  67575,  90336,  93307,
        96825,  99001, 102369, 127442, 158167, 170532, 172184, 175121,
       182053, 193514, 202318, 204184, 205484, 228974, 231146, 259660,
       274823, 275738, 283542, 285766, 293069, 299638,
       303746, 314061, 330703, 334269, 340136, 354575, 372900, 375667,
       379363, 382553, 390997, 395101]

num_inputs = u_exact.shape[1]
Nx = u_exact.shape[1]
batches = int(1) # number of batches for training
stime = int(100) # total sample size for restarting - should be small for restart
horizon = int(1500) # duration of the prediction
### Treatment of output ------------------------------------------------------------
num_outputs = Nx

### LOAD THE PARAMETERS OF THE SAVED ESN
train_time = int(10000)
num_units_list = [1000]


lmblist = [1e-6]
rholist = [0.9]
decaylist = [1.0]
sigma_inlist = [1.0]
phys = True
valid_hor = 5000

decay = decaylist[0] # for leakage (not used here)
rho_spectral = rholist[0] # 
sigma_in = sigma_inlist[0]# 
lmb = lmblist[0]
dir_name = './result/'

for ii_units in range(len(num_units_list)):
    num_units = num_units_list[ii_units]
    print(num_units)

    if (phys):
        fln_mod = dir_name  + case + '_ESN_train' + str(train_time) + '_' + str(num_units) + 'units_phys_lmb' + str(lmb) + '_validhor_' + str(valid_hor) + '_mod.h5'
    else:
        fln_mod = dir_name + case + '_ESN_train' + str(train_time) + '_' + str(num_units) + 'units_lmb' + str(lmb) + '_nophys_mod.h5'

    hf_mod = h5py.File(fln_mod,'r')

    win = np.array(hf_mod.get('/Win'))
    wecho = np.array(hf_mod.get('/Wecho'))
    Wout = np.array(hf_mod.get('/Wout'))

    num_units = np.array(hf_mod.get('/num_units'))
    num_inputs = np.array(hf_mod.get('/num_inputs'))
    rho_ESN = np.array(hf_mod.get('/rho'))
    sigma_in = np.array(hf_mod.get('/sigma_in'))
    sparseness = np.array(hf_mod.get('/sparseness'))
    decay = np.array(hf_mod.get('/decay'))

    st_saved = np.array(hf_mod.get('/state')) # /!\ If prediction starts right after the saved stated then use this state
                                        # Otherwise, need to manually restart the ESN with a sequence of inputs to have appropriate states
    pred = np.array(hf_mod.get('/pred'))

    lmb = np.array(hf_mod.get('/lmb'))
    hf_mod.close()

    ### 
    save = False # save the training/validation/prediction curves

    activation = lambda x: math_ops.tanh(x) # the activation function of the ESN

    ### tensorflow graph -------------------------------------------------------------

    rnn_init_state = np.zeros([batches, num_units], dtype="float64")

    tf.reset_default_graph()
    graph = tf.Graph()

    with graph.as_default() as g:    
        rng = np.random.RandomState(random_seed)
        # Init the ESN cell
        cell = EchoStateRNNCell(num_units=num_units, 
                            num_inputs=num_inputs,
                            activation=activation, 
                            decay=decay, # decay (leakage) rate
                            rho=rho_ESN, # spectral radius of echo matrix
                            sigma_in=sigma_in, # scaling of input matrix
                            sigma_echo=1.0,# scaling of echo matrix
                            sparseness = sparseness, # sparsity of the echo matrix
                            rng=rng,
                            reuse=True,
                            win=win,
                            wecho=wecho)
    
        # Build the graph for small re-initialisation of the ESN
        init_state = tf.placeholder(tf.float64, [1, num_units])
        inputs = tf.placeholder(tf.float64, [batches, stime, num_inputs])
        print( "Initialize graph for restart")
        states,statesLastTrain = tf.nn.dynamic_rnn(cell=cell,inputs=inputs,initial_state=init_state,dtype=tf.float64)
        outputs1 = tf.reshape(states, [stime, num_units])   
        outputs2 = tf.square(outputs1)
    
        k1 = np.zeros((stime,num_units))
        k1[:,np.arange(0,num_units,2)] = 1.0
        k1_tf = tf.convert_to_tensor(k1,np.float64)
        k2 = np.zeros((stime,num_units))
        k2[:,np.arange(1,num_units,2)] = 1.0
        k2_tf = tf.convert_to_tensor(k2,np.float64)
        outputs = tf.add( tf.multiply(outputs1,k1_tf) , tf.multiply(outputs2,k2_tf) )
    
        Wout_tf = tf.Variable(np.random.rand(num_units,num_outputs),dtype=tf.float64,trainable=True)
        Y_tf = tf.matmul(outputs,Wout_tf)
    
        # Build the graph extension for prediction (1 step first and then horizon)
        inputF = tf.placeholder(tf.float64, [1, num_inputs])
        stateF = tf.placeholder(tf.float64, [1, num_units])
    
        stateL1,_ = cell(inputs=inputF, state=stateF)
        stateL2 = tf.square(stateL1)
        # mask for square of state on 1 timestep
        k1_pred = np.zeros((1,num_units))
        k1_pred[:,np.arange(0,num_units,2)] = 1.0
        k1_pred_tf = tf.convert_to_tensor(k1_pred,np.float64)
        k2_pred = np.zeros((1,num_units))
        k2_pred[:,np.arange(1,num_units,2)] = 1.0
        k2_pred_tf = tf.convert_to_tensor(k2_pred,np.float64)
    
        # new 1step state and autonomous prediction
        stateL = tf.add( tf.multiply(stateL1,k1_pred_tf) , tf.multiply(stateL2,k2_pred_tf) )
        Ypred_tf = tf.matmul(stateL,Wout_tf)
    
        state_pred1 = stateL1
        Ypred_all_tf = []
        Ypred_all_tf.append(Ypred_tf)
        for t in range(1,horizon):
            state_pred1,_ = cell(inputs=Ypred_tf,state=state_pred1)
            state_pred2 = tf.square(state_pred1)
            state_pred = tf.add( tf.multiply(state_pred1,k1_pred_tf) , tf.multiply(state_pred2,k2_pred_tf) )
            Ypred_tf = tf.matmul(state_pred,Wout_tf)
            Ypred_all_tf.append(Ypred_tf)
        outputs_pred = tf.reshape(Ypred_all_tf, [horizon, num_outputs])
    
    
        # Run the small restart and prediction
        with tf.Session(config=config) as sess:
        
            sess.run(tf.global_variables_initializer())
            Wout_tf.load(Wout,sess)
            valid_t = np.zeros(len(i_RE))
        
            for iitime in range(len(i_RE)):
                ## Parameters for the split of the training/validation ----------------------------
                cut1 = i_RE[iitime] 
                cut2 = i_RE[iitime] + stime+horizon+1 
                uu = u_exact[np.arange(cut1,cut2),:]

                ### Treatment of inputs -----------------------------------------------------------
                rnn_inputs = np.zeros((batches, stime, num_inputs), dtype="float64")
                wave = uu[:stime+1,:].astype("float64")
                rnn_inputs = wave[:-1,:].reshape(1,stime, num_inputs)
                del wave


                Yout,st  = sess.run([Y_tf,statesLastTrain], feed_dict={inputs:rnn_inputs,
                                                        init_state:rnn_init_state})
                inn = Yout[-1,:]
            
                # future prediction
                inn = inn.reshape((1,num_inputs)).astype('float64')
                Ypred = sess.run(outputs_pred, feed_dict={inputF:inn,stateF:st})
            
                err_pred = np.sqrt( np.sum((Ypred-uu[stime+1:stime+horizon+1,:])**2.,1) ) \
                    / np.sqrt( np.mean(np.sum(uu[stime+1:stime+horizon+1,:]**2.,1) ) )
                valid_t[iitime] = np.argmax(err_pred>0.2)
                if (valid_t[iitime]==0 and err_pred[0]>0.2):
                    valid_t[iitime] = -1
                elif (valid_t[iitime]==0 and err_pred[0]<0.2):
                    valid_t[iitime] = horizon
            
                valid_t[iitime] = valid_t[iitime]*dt
                print('calculating valid time, case# ', iitime, ' :', valid_t[iitime])
                if (phys):
                    fln_save = dir_name + case + '_ESN_train' + str(train_time) + '_' + str(num_units) + '_lmb' + str(lmb) + 'units_phys' + \
                    '_validhor_' + str(valid_hor) + '_RE.h5'
                else:

                    fln_save = dir_name + case + '_ESN_train' + str(train_time) + '_' + str(num_units) + '_lmb' + str(lmb) + 'units_nophys' + \
                    '_RE.h5'

                hf = h5py.File(fln_save,'a')
                hf.create_dataset('Ypred' + str(iitime),data=Ypred)
                hf.create_dataset('data' + str(iitime),data=uu[stime+1:stime+horizon+1,:])
                hf.create_dataset('err_pred' + str(iitime),data=err_pred)
                hf.close()
