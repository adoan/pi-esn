# NAKD
"""
Last Update: 26/07/2021
    Main routine to train the ESN given a noisy database of the MFE system

"""

import numpy as np
import tensorflow as tf
from tensorflow.python.ops import math_ops
from ESN_Chaos import EchoStateRNNCell

# import matplotlib.pyplot as plt
import time
import h5py

import datetime

from scipy import optimize


def MFE_RHS_tf(u, zeta,xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9):
    
    RHS = - tf.matmul(u,zeta)
    [u1,u2,u3,u4,u5,u6,u7,u8,u9] = tf.unstack(RHS,axis=1)
    
    u1 = u1 + zeta[0,0] - xi1[0]*u[:,5]*u[:,7] + xi1[1]*u[:,1]*u[:,2]
    
    u2 = u2 + xi2[0]*u[:,3]*u[:,5] - xi2[1]*u[:,4]*u[:,6] - xi2[2]*u[:,4]*u[:,7] - xi2[3]*u[:,0]*u[:,2] - xi2[4]*u[:,2]*u[:,8]
    u3 = u3 + xi3[0]*(u[:,3]*u[:,6] + u[:,4]*u[:,5] ) + xi3[1]*u[:,3]*u[:,7]
    u4 = u4 - xi4[0]*u[:,0]*u[:,4] - xi4[1]*u[:,1]*u[:,5] - xi4[2]*u[:,2]*u[:,6] - xi4[3]*u[:,2]*u[:,7] - xi4[4]*u[:,4]*u[:,8]
    u5 = u5 + xi5[0]*u[:,0]*u[:,3] + xi5[1]*u[:,1]*u[:,6] - xi5[2]*u[:,1]*u[:,7] + xi5[3]*u[:,3]*u[:,8] + xi5[4]*u[:,2]*u[:,5]
    u6 = u6 + xi6[0]*u[:,0]*u[:,6] + xi6[1]*u[:,0]*u[:,7] + xi6[2]*u[:,1]*u[:,3] - xi6[3]*u[:,2]*u[:,4] + xi6[4]*u[:,6]*u[:,8] + xi6[5]*u[:,7]*u[:,8]
    u7 = u7 - xi7[0]*(u[:,0]*u[:,5] + u[:,5]*u[:,8] ) + xi7[1]*u[:,1]*u[:,4] + xi7[2]*u[:,2]*u[:,3]
    u8 = u8 + xi8[0]*u[:,1]*u[:,4] + xi8[1]*u[:,2]*u[:,3]
    u9 = u9 + xi9[0]*u[:,1]*u[:,2] - xi9[1]*u[:,5]*u[:,7]
    
    return tf.stack([u1,u2,u3,u4,u5,u6,u7,u8,u9],axis=1)

def MFE_step_tf(Uin, zeta,xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9, dt):
    
    #Y = np.zeros(Uin.shape)
    k1 = dt*MFE_RHS_tf(Uin, zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    k2 = dt*MFE_RHS_tf(Uin+k1/3., zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    k3 = dt*MFE_RHS_tf(Uin-k1/3.+k2, zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    k4 = dt*MFE_RHS_tf(Uin+k1-k2+k3, zeta, xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9)
    
    Y = Uin + (k1 + 3.*k2 + 3.*k3 + k4) / 8.
    return Y


# Configs ----------------------------------------------------------------------

# takes only current needed GPU memory
tf.logging.set_verbosity(tf.logging.ERROR)
#config = tf.ConfigProto()
config = tf.ConfigProto(log_device_placement=True)
t1_1 = datetime.datetime.now()
config.gpu_options.allow_growth = True
random_seed = 1

### -------- DATASET ------------------- 
SNR = 30 # noise level

## for MFE model
hf = h5py.File('../../Data/MFE_Re600_DATA.h5','r') # this data has 20000 timestep, DT=0.25, T=5000 and Nx=48
case = 'MFE'
t = np.array(hf.get('t'))
dt = np.array(hf.get('dt'))

# physical parameters for the MFE model
Lx = np.array(hf.get('Lx'))
Lz = np.array(hf.get('Lz'))
alpha = np.array(hf.get('alpha'))
beta = np.array(hf.get('beta'))
gamma_mod = np.array(hf.get('gamma'))
zeta = np.array(hf.get('zeta'))
xiall = []
for i in range(1,10):
    var = 'xi' + str(i)
    xiall.append( np.array(hf.get(var)) )
    
[xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9] = xiall
del xiall

hf.close()

hf = h5py.File('../../Data/MFE_Re600_DATA_noiseSNR' + str(SNR) + '.h5','r')
u_exact = np.array(hf.get('u'))
u_nonoise = np.array(hf.get('u_nonoise'))
hf.close()

num_inputs = u_exact.shape[1]
Nx = u_exact.shape[1]

## Parameters for the split of the training/validation ----------------------------
jumpT = int(1) # to under-sample in time
batches = int(1) # number of batches for training
stime = int(10000/jumpT) # total sample size for training + validation

begin = int(100/jumpT) # begin of training (wash out at the start to remove the transient of the reservoir)
end = int(10000/jumpT) # has to be smaller than stime

valid_hor = 5000 
horizon = int(valid_hor/jumpT) # duration of the prediction

cut1 = int(3000+43000) # 
cut2 = int(3000+43000+jumpT*stime+jumpT*horizon+1) # 
u_exact = u_exact[np.arange(cut1,cut2,jumpT),:] # 
dt = jumpT*dt

### Treatment of inputs -----------------------------------------------------------
rnn_inputs = np.zeros((batches, stime, num_inputs), dtype="float64")
wave = u_exact[:stime+1,:].astype("float64")

rnn_inputs = wave[:-1,:].reshape(1,stime, num_inputs)

### Treatment of output ------------------------------------------------------------
num_outputs = Nx 
rnn_target = wave[1:,:]
rnn_target = rnn_target.reshape(stime, num_outputs).astype("float64")

del wave

### META PARAMETERS of ESN --------------------------------------------------------
# Parameter to generate the matrix of the ESN
num_units_list = [1000]
lossphys = 1.0
phys = True # if physics informed optimization
save = True # save the training/validation/prediction curves
save_model = True # save the model matrix
lmblist = [1e-6] #
rholist = [0.9] # 
decaylist = [1.0] # 
sigma_inlist = [1.0] #
activation = lambda x: math_ops.tanh(x) # the activation function of the ESN

for ii_units in range(len(num_units_list)):
    num_units = num_units_list[ii_units] # num_units_list[ii_units]
    decay = decaylist[0] # for leakage (not used in PRL/chaos paper)
    rho_spectral = rholist[0] # spectral radius of Wecho
    sigma_in = sigma_inlist[0]# scaling of input weight
    sigma_echo = 1.0 # initial scaling for Wecho (useless as everything is rescaled by rho...)
    sparseness = 1. - 3. / (num_units - 1.) # sparseness of Wecho
    ## it's defined as 1 - degree / (num_units - 1) where degree is the average number of connections of a unit to other units in reservoir
    lmb = lmblist[0] # tikhonov regularization

    rnn_init_state = np.zeros([batches, num_units], dtype="float64")
    ### tensorflow graph -------------------------------------------------------------

    tf.reset_default_graph()
    graph = tf.Graph()

    with graph.as_default() as g:    
        rng = np.random.RandomState(random_seed)

        # Init the ESN cell
        cell = EchoStateRNNCell(num_units=num_units, 
                                num_inputs=num_inputs,
                                activation=activation, 
                                decay=decay, # decay (leakage) rate
                                rho=rho_spectral, # spectral radius of echo matrix
                                sigma_in=sigma_in, # scaling of input matrix
                                sigma_echo=sigma_echo,# scaling of echo matrix
                                sparseness = sparseness, # sparsity of the echo matrix
                                rng=rng)
        
        inputs = tf.placeholder(tf.float64, [batches, stime, num_inputs]) # inputs with 1-bias (see Lukocevicius 2012)
        target = tf.placeholder(tf.float64, [stime, num_outputs])
        
        init_state = tf.placeholder(tf.float64, [1, num_units])
        
        # Build the graph (for training on a time sequence)
        print("Initialize graph")
        states,statesLastTrain = tf.nn.dynamic_rnn(cell=cell,inputs=inputs,initial_state=init_state,dtype=tf.float64)
        
        outputs1 = tf.reshape(states, [stime, num_units])   
        outputs2 = tf.square(outputs1)
        
        # Quadratic transformation as in Pathak2018
        k1 = np.zeros((stime,num_units))
        k1[:,np.arange(0,num_units,2)] = 1.0
        k1_tf = tf.convert_to_tensor(k1,np.float64)
        k2 = np.zeros((stime,num_units))
        k2[:,np.arange(1,num_units,2)] = 1.0
        k2_tf = tf.convert_to_tensor(k2,np.float64)
        outputs = tf.add( tf.multiply(outputs1,k1_tf) , tf.multiply(outputs2,k2_tf) )
        
        # 
        Wout_tf = tf.Variable(tf.zeros([num_units, num_outputs], dtype=tf.float64) )
        
        Y_tf = tf.matmul(outputs[begin:end,:],Wout_tf)
        inputF = tf.reshape(Y_tf[-1,:],[1, num_inputs])
        
        # Build the graph extension for a single timestep and future autonomous prediction
        #inputF = tf.placeholder(tf.float64, [1, num_inputs])
        stateF = tf.placeholder(tf.float64, [1, num_units])
        stateL1,_ = cell(inputs=inputF, state=stateF)
        stateL2 = tf.square(stateL1)
        
        # mask for square of state on 1 timestep
        k1_pred = np.zeros((1,num_units))
        k1_pred[:,np.arange(0,num_units,2)] = 1.0
        k1_pred_tf = tf.convert_to_tensor(k1_pred,np.float64)
        k2_pred = np.zeros((1,num_units))
        k2_pred[:,np.arange(1,num_units,2)] = 1.0
        k2_pred_tf = tf.convert_to_tensor(k2_pred,np.float64)
        
        # new 1step state and autonomous prediction
        stateL = tf.add( tf.multiply(stateL1,k1_pred_tf) , tf.multiply(stateL2,k2_pred_tf) )
        
        Ypred_tf = tf.matmul(stateL,Wout_tf)
        
        state_pred1 = stateL1
        Ypred_all_tf = []
        Ypred_all_tf.append(Ypred_tf)
        for t in range(1,horizon):
            state_pred1,_ = cell(inputs=Ypred_tf,state=state_pred1)
            state_pred2 = tf.square(state_pred1)
            state_pred = tf.add( tf.multiply(state_pred1,k1_pred_tf) , tf.multiply(state_pred2,k2_pred_tf) )
            Ypred_tf = tf.matmul(state_pred,Wout_tf)
            Ypred_all_tf.append(Ypred_tf)
            
        outputs_pred = tf.reshape(Ypred_all_tf, [horizon, num_outputs])
        

        Yval_exact_tf = MFE_step_tf(outputs_pred[:-1,:],zeta,xi1,xi2,xi3,xi4,xi5,xi6,xi7,xi8,xi9, dt)
        
        Ytarget_tf = tf.placeholder(tf.float64,[end-begin, num_outputs])
        
        loss_err_tf = tf.placeholder(tf.float64)
        loss_phys_tf = tf.placeholder(tf.float64)

        LOSS_TF = loss_err_tf*tf.reduce_mean( tf.reduce_mean( tf.square(Y_tf - Ytarget_tf), 1 ) ) + \
            loss_phys_tf*tf.reduce_mean( tf.reduce_mean( tf.square(outputs_pred[1:,:] - Yval_exact_tf),1 ) )
        
        # Create the optimizer
        optimizer = tf.contrib.opt.ScipyOptimizerInterface(LOSS_TF,
                method = 'L-BFGS-B',var_list=[Wout_tf],options = {'maxfun': 50000, 'maxiter':50000,
                                            'maxcor':50,'maxls':50,
                                                'eps': 1.0 * np.finfo(float).eps,
                                                'ftol' : 1.0 * np.finfo(float).eps})
        
        # Run the simulation
        sess = tf.Session(config=config)
        sess.run(tf.global_variables_initializer())
        
        print("Start states calculations")
        stored_outputs,st  = sess.run([outputs,statesLastTrain], feed_dict={inputs:rnn_inputs,
                                                init_state:rnn_init_state})
        t2_1 = datetime.datetime.now()

        st =  np.reshape(st[-1,:],[1, num_units]).astype('float64') # save the last state for future prediction
        
        output_slice = stored_outputs[begin:end,:].astype('float64') # training is done without the beginning reservoir states transient (wash out)
        XX_train = output_slice.astype('float64') # [states] stime lines x (num_inputs + num_units)
        
        if (phys):
            fln = './SNR' + str(SNR) + '/' + case + '_ESN_train' + str(stime) + '_' + str(num_units) + 'units_phys_lossphys' + str(lossphys) + '_lmb' + str(lmb) + '_validhor_' + str(valid_hor) + '.h5'
            fln_mod = './SNR' + str(SNR) + '/' + case + '_ESN_train' + str(stime) + '_' + str(num_units) + 'units_phys_lossphys' + str(lossphys) + '_lmb' + str(lmb) + '_validhor_' + str(valid_hor) + '_mod.h5'
        else:
            fln = './SNR' + str(SNR) + '/' + case + '_ESN_train' + str(stime) + '_' + str(num_units) + '_lmb' + str(lmb) + 'units_nophys_' + str(0) + '.h5'
            fln_mod = './SNR' + str(SNR) + '/' + case + '_ESN_train' + str(stime) + '_' + str(num_units) + '_lmb' + str(lmb) + 'units_nophys_' + str(0) + 'mod.h5'
            
        if (save):
            hf = h5py.File(fln,'w')
            
        print( "Optimize Wout")

        Wout = np.matmul( 
            np.linalg.inv(np.matmul(np.transpose(XX_train), XX_train) +
                lmb*np.eye(num_units)),
            np.matmul(np.transpose(XX_train), rnn_target[begin:end,:]) )
        Wout_save = Wout.astype('float64')
        Wout_tf.load(Wout,sess)
        
        ### Change here the calculation of Wout to an optimization problem for physics informed
        if (phys==True):
            Wout00 = Wout.astype('float64')
            Wout_save = Wout.astype('float64')
            [LOSS00] = sess.run([LOSS_TF],feed_dict={Ytarget_tf:rnn_target[begin:end,:],stateF:st,outputs:stored_outputs,
                                                                    loss_err_tf:1.0,loss_phys_tf:lossphys})
            LOSS0 = LOSS00
            print("Optimizing Wout")
            opt_res = optimizer.minimize(sess,feed_dict ={Ytarget_tf:rnn_target[begin:end,:],stateF:st,outputs:stored_outputs,
                                                            loss_err_tf:1.0,loss_phys_tf:lossphys})
            [Yout,Wout0,LOSS0] = sess.run([Y_tf,Wout_tf,LOSS_TF],feed_dict={Ytarget_tf:rnn_target[begin:end,:],stateF:st,outputs:stored_outputs,
                                                            loss_err_tf:1.0,loss_phys_tf:lossphys})
            Wout_save = Wout[:]
            # this loop below may be necessary to nudge the optimization away from the initial guess and explore the optimization space more widely.
            # the physics-based optimization require a more hands on optimization approach but provide directions to get potential improvements in Wout
            # This can be done with multiple starting candidates Wout initialized differently.
            for iperturb in range(-2,3):
                Wout0 = np.matmul( np.linalg.inv(np.matmul(np.transpose(XX_train), XX_train) +
                    (lmb*3.**(iperturb))*np.eye(num_units)),
                    np.matmul(np.transpose(XX_train), rnn_target[begin:end,:]) )
                Wout_tf.load(Wout0,sess)
                Wout = Wout0.astype('float64')
                for imodif in range(3):
                    opt_res = optimizer.minimize(sess,feed_dict ={Ytarget_tf:rnn_target[begin:end,:],stateF:st,outputs:stored_outputs,
                                                                    loss_err_tf:1.0,loss_phys_tf:lossphys})
                    [Yout,Wout,LOSS] = sess.run([Y_tf,Wout_tf,LOSS_TF],feed_dict={Ytarget_tf:rnn_target[begin:end,:],stateF:st,outputs:stored_outputs,
                                                                    loss_err_tf:1.0,loss_phys_tf:lossphys})
                    if LOSS<LOSS0:
                        Wout_save = Wout[:]
                        LOSS0 = LOSS
                        break
                    Wout = np.multiply(Wout0, np.ones(np.shape(Wout0)) + (1e-6 * 10**imodif) * ((np.random.random(np.shape(Wout0))-0.5)*2.) )
                    Wout_tf.load(Wout,sess)
        
        print("Finished calculating Wout")
        Wout_tf.load(Wout_save,sess)
        [Yout,Wout] = sess.run([Y_tf,Wout_tf],feed_dict={outputs:stored_outputs})
        
        err_train = np.mean((Yout-rnn_target[begin:end,:])**2.)
        
        if (save):
            hf.create_dataset('Yout_train',data=Yout)
            hf.create_dataset('Target_train',data=rnn_target[begin:end,:])
            hf.create_dataset('err_train',data=err_train)
            
        inn = Yout[-1,:]
         
        # future autonomous prediction
        inn = inn.reshape((1,num_outputs)).astype('float64')
        
        Ypred = np.zeros((horizon,num_outputs))
        Ypred = sess.run(outputs_pred,feed_dict={stateF:st, inputF:inn})
        err_pred = np.sum((Ypred-u_exact[stime+1:stime+horizon+1,:num_outputs])**2.,1)  / np.mean((u_exact[stime+1:stime+horizon+1,:num_outputs]**2.))
        
        if (save):
            hf.create_dataset('err_pred',data=err_pred)
            hf.create_dataset('Yout_pred',data=Ypred)
            hf.create_dataset('Target_pred',data=u_exact[stime+1:stime+horizon+1,:])
            hf.close()
            
        if (save_model):
            cell.save_ESN(fln_mod,Wout,st,lmb)

t2_2 = datetime.datetime.now()
print(t2_1-t1_1)
print(t2_2-t2_1)    
